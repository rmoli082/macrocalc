MacroCalc is a small app written in Java that will help to calculate the correct amount of calories, carbs, protein and fat for your weight loss or gain diet. Enter your age, height, weight and bodyfat percentages (if known) and select some lifestyle options to determine the right amount of food to eat every day!

To Do:
- Refresh the layout
- Add descriptive text