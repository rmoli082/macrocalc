package org.openjfx;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.geometry.Insets;

public class MainApp extends Application {
	BorderPane pane = new BorderPane();
	Scene scene = new Scene(pane, 1200, 800);
	
	TextField nameEntry = new TextField();
	TextField ageEntry = new TextField();
	TextField heightEntry = new TextField();
	TextField weightEntry = new TextField();
	TextField bfpEntry = new TextField();

	RadioButton male = new RadioButton("M");
	RadioButton female = new RadioButton("F");
	
	Button calculateBFP = new Button("Calculate Bodyfat %");
	
	RadioButton sedentary = new RadioButton("Sedentary");
	RadioButton light = new RadioButton("Light");
	RadioButton medium = new RadioButton("Medium");
	RadioButton heavy = new RadioButton("Heavy");
	RadioButton intense = new RadioButton("Intense");
	
	RadioButton iifym = new RadioButton("IIFYM");
	RadioButton keto = new RadioButton("Keto");
	RadioButton paleo = new RadioButton("Low Carb");
	RadioButton lowfat = new RadioButton("Low Fat");
	
	RadioButton gain = new RadioButton("Gain Muscle");
	RadioButton lose = new RadioButton("Lose Fat");
	
	RadioButton mild = new RadioButton("Light");
	RadioButton normal = new RadioButton("Normal");
	RadioButton aggressive = new RadioButton("Aggressive");
	
	TextField tdeeCalc = new TextField();
	TextField totalCalc = new TextField();
	TextField proteinGramsEntry = new TextField();
	TextField fatGramsEntry = new TextField();
	TextField carbGramsEntry = new TextField();
	
	double activityLevelModifier = 0.0;
	double age = 0;
	double height = 0;
	double weight = 0.0;
	double bfp = 0.0;
	double modifier = 0.0;
	double tdee = 0.0;
	double totalCalories = 0.0;
	int proteinGrams;
	int fatGrams;
	int carbGrams;
	
	double bfpHeight = 0;
	double bfpNeck = 0;
	double bfpWaist = 0;
	double bfpHips = 0;
	
	Stage stage2 = new Stage();
	GridPane gpane  = new GridPane();
	RadioButton malebfp = new RadioButton("M");
	RadioButton femalebfp = new RadioButton("F");
	TextField bfpheightEntry = new TextField();
	TextField neckEntry = new TextField();
	TextField waistEntry = new TextField();
	TextField hipsEntry = new TextField();
	
	@Override
	public void start(Stage primaryStage) {
		
		GridPane topStats = new GridPane();
		topStats.setAlignment(Pos.CENTER);
		topStats.setHgap(5);
		topStats.setPadding(new Insets(15,15,10,15));
		topStats.add(new Label("Enter Name:"), 0, 0);
		nameEntry.setPrefColumnCount(10);
		topStats.add(nameEntry, 1, 0);
		topStats.add(new Label("Enter Age: "), 2, 0);
		ageEntry.setPrefColumnCount(4);
		topStats.add(ageEntry, 3, 0);
		topStats.add(new Label("Enter Height (inches): "), 0, 1);
		heightEntry.setPrefColumnCount(4);
		topStats.add(heightEntry, 1, 1);
		topStats.add(new Label("Enter Weight (lbs): "), 2, 1);
		weightEntry.setPrefColumnCount(5);
		topStats.add(weightEntry, 3, 1);
		topStats.add(new Label("Enter Bodyfat %: "), 4, 1);
		bfpEntry.setPrefColumnCount(4);
		topStats.add(bfpEntry, 5, 1);
		HBox topgender = new HBox();
		topgender.getChildren().addAll(new Label("Sex: "), male, female);
		topStats.add(topgender, 0, 2);
		ToggleGroup gender = new ToggleGroup();
		male.setToggleGroup(gender);
		female.setToggleGroup(gender);
		male.setOnAction(e -> {
			if (male.isSelected()) {
			modifier = 5;
		}
			});
		female.setOnAction(e -> {
			if (female.isSelected()) {
			modifier = -161;
		}
			});
		topStats.add(calculateBFP, 6, 1);
		calculateBFP.setOnAction(e -> calculateBFPwindow());
		
		VBox sideActivity = new VBox(20);
		sideActivity.setPadding(new Insets(10, 15, 15, 15));
		
		VBox activityLevel = new VBox(15);
		Label title1 = new Label("Select Activity Level: ");
		ToggleGroup activity = new ToggleGroup();
		sedentary.setToggleGroup(activity);
		light.setToggleGroup(activity);
		medium.setToggleGroup(activity);
		heavy.setToggleGroup(activity);
		intense.setToggleGroup(activity);
		sedentary.setOnAction(e -> {
			if (sedentary.isSelected()) {
			activityLevelModifier = 1.1;
		}
			});
		light.setOnAction(e -> {
			if (light.isSelected()) {
			activityLevelModifier = 1.3;
		}
			});
		medium.setOnAction(e -> {
			if (medium.isSelected()) {
			activityLevelModifier = 1.5;
		}
			});
		heavy.setOnAction(e -> {
			if (heavy.isSelected()) {
			activityLevelModifier = 1.7;
		}
			});
		intense.setOnAction(e -> {
			if (intense.isSelected()) {
			activityLevelModifier = 1.9;
		}
			});
		activityLevel.getChildren().addAll(title1, sedentary, light, medium, 
				heavy, intense);
		
		VBox dietaryPlan = new VBox(15);
		Label title2 = new Label("Select Diet Plan: ");
		ToggleGroup diet = new ToggleGroup();
		iifym.setToggleGroup(diet);
		keto.setToggleGroup(diet);
		paleo.setToggleGroup(diet);
		lowfat.setToggleGroup(diet);
		dietaryPlan.getChildren().addAll(title2, iifym, keto, paleo, lowfat);
		
		VBox primaryGoal = new VBox(15);
		Label title3 = new Label("Select Primary Goal: ");
		ToggleGroup goal = new ToggleGroup();
		gain.setToggleGroup(goal);
		lose.setToggleGroup(goal);
		primaryGoal.getChildren().addAll(title3, gain, lose);
		
		VBox intensityModifier = new VBox(15);
		Label title4 = new Label("Select intensity");
		ToggleGroup intensity = new ToggleGroup();
		mild.setToggleGroup(intensity);
		normal.setToggleGroup(intensity);
		aggressive.setToggleGroup(intensity);
		intensityModifier.getChildren().addAll(title4, mild, normal, aggressive);
		
		Button calculateButton = new Button("Calculate Macros");
		calculateButton.setOnAction(e -> calculateMacros());
		
		sideActivity.getChildren().addAll(activityLevel, dietaryPlan, primaryGoal, 
				intensityModifier, calculateButton);
		
		GridPane centerPane = new GridPane();
		centerPane.setAlignment(Pos.CENTER);
		centerPane.setHgap(5);
		centerPane.setPadding(new Insets(15,15,15,15));
		centerPane.add(new Label("TDEE: "), 0, 0);
		tdeeCalc.setPrefColumnCount(8);
		tdeeCalc.setEditable(false);
		centerPane.add(tdeeCalc, 1, 0);
		centerPane.add(new Label("Total Daily Calories"), 2, 0);
		totalCalc.setPrefColumnCount(8);
		totalCalc.setEditable(false);
		centerPane.add(totalCalc, 3, 0);
		centerPane.add(new Label(""), 0, 1);
		centerPane.add(new Label(""), 0, 2);
		centerPane.add(new Label("Carb Grams: "), 0, 3);
		carbGramsEntry.setEditable(false);
		centerPane.add(carbGramsEntry, 1, 3);
		centerPane.add(new Label("Protein Grams: "), 0, 4);
		proteinGramsEntry.setEditable(false);
		centerPane.add(proteinGramsEntry, 1, 4);
		centerPane.add(new Label("Fat Grams"), 0, 5);
		fatGramsEntry.setEditable(false);
		centerPane.add(fatGramsEntry, 1, 5);
		
		pane.setTop(topStats);
		pane.setLeft(sideActivity);
		pane.setCenter(centerPane);
		primaryStage.setTitle("Calculate Calorie Needs");
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	public void calculateBFPwindow() {
		
		gpane.setPadding(new Insets (15, 15, 15, 15));
		gpane.setHgap(5);
		Scene scene2 = new Scene(gpane, 280, 200);
		
		Button calcbfp = new Button("Calculate");
		calcbfp.setOnAction(e -> calculateBFP());
		
		GridPane measurements = new GridPane();
		HBox gender = new HBox();
		Label gendertag = new Label("Select gender: ");
		ToggleGroup genderbfp = new ToggleGroup();
		malebfp.setToggleGroup(genderbfp);
		femalebfp.setToggleGroup(genderbfp);
		
		femalebfp.setOnAction(e -> {if (femalebfp.isSelected()) {
			measurements.add(new Label("Hips"), 0, 4);
			measurements.add(hipsEntry, 1, 4);
		}});
		gender.getChildren().addAll(gendertag, malebfp, femalebfp);
		
		measurements.setHgap(7);
		measurements.add(new Label("Enter measurement in inches"), 1, 0);
		measurements.add(new Label("Height"), 0, 1);
		measurements.add(bfpheightEntry, 1, 1);
		measurements.add(new Label("Neck"), 0, 2);
		measurements.add(neckEntry, 1, 2);
		measurements.add(new Label("Waist"), 0, 3);
		measurements.add(waistEntry, 1, 3);
		measurements.add(new Label(" "), 0, 4);
		measurements.add(calcbfp, 1, 5);
			
		gpane.add(gender, 0, 0);
		gpane.add(measurements, 0, 1);
		stage2.setScene(scene2);
		stage2.show();
	}
	
	public void calculateMacros() {
			age = Double.parseDouble(ageEntry.getText());
			height = Double.parseDouble(heightEntry.getText());
			weight = Double.parseDouble(weightEntry.getText());
			if (bfpEntry.getText().length() != 0){
				bfp = Double.parseDouble(bfpEntry.getText());
			}
			
			if (bfpEntry.getText().length() != 0) {
				tdee = (370 + (21.6 * (weight/2.2)* (1 - (bfp / 100)))) 
						* activityLevelModifier;
			}
			else {
				tdee = ((10 * (weight/2.2)) + (6.25 * (height*2.54)) - (5 * age) + 
						modifier) * activityLevelModifier;
			}
			
			if (gain.isSelected()) {
				if (mild.isSelected()) {
					totalCalories = tdee * 1.15;
				}
				if (normal.isSelected()) {
					totalCalories = tdee * 1.2;
				}
				if (aggressive.isSelected()) {
					totalCalories = tdee * 1.25;
				}
				if (iifym.isSelected()) {
					carbGrams = (int) (totalCalories * 0.5 / 4);
					proteinGrams = (int) (totalCalories * 0.2 / 4);
					fatGrams = (int) (totalCalories * 0.3 / 9);
					carbGramsEntry.setText(Integer.toString(carbGrams));
					proteinGramsEntry.setText(Integer.toString(proteinGrams));
					fatGramsEntry.setText(Integer.toString(fatGrams));
				}
				if (keto.isSelected()) {
					carbGrams = (int) (totalCalories * 0.15 / 4);
					proteinGrams = (int) (totalCalories * 0.15 / 4);
					fatGrams = (int) (totalCalories * 0.7 / 9);
					carbGramsEntry.setText(Integer.toString(carbGrams));
					proteinGramsEntry.setText(Integer.toString(proteinGrams));
					fatGramsEntry.setText(Integer.toString(fatGrams));
				}
				if (paleo.isSelected()) {
					carbGrams = (int) (totalCalories * 0.3 / 4);
					proteinGrams = (int) (totalCalories * 0.3 / 4);
					fatGrams = (int) (totalCalories * 0.4 / 9);
					carbGramsEntry.setText(Integer.toString(carbGrams));
					proteinGramsEntry.setText(Integer.toString(proteinGrams));
					fatGramsEntry.setText(Integer.toString(fatGrams));
				}
				if (lowfat.isSelected()) {
					carbGrams = (int) (totalCalories * 0.5 / 4);
					proteinGrams = (int) (totalCalories * 0.35 / 4);
					fatGrams = (int) (totalCalories * 0.15 / 9);
					carbGramsEntry.setText(Integer.toString(carbGrams));
					proteinGramsEntry.setText(Integer.toString(proteinGrams));
					fatGramsEntry.setText(Integer.toString(fatGrams));
				}
			}
			if (lose.isSelected()) {
				if (mild.isSelected()) {
					totalCalories = tdee / 1.15;
				}
				if (normal.isSelected()) {
					totalCalories = tdee / 1.2;
				}
				if (aggressive.isSelected()) {
					totalCalories = tdee / 1.25;
				}
				if (iifym.isSelected()) {
					carbGrams = (int) (totalCalories * 0.35 / 4);
					proteinGrams = (int) (totalCalories * 0.35 / 4);
					fatGrams = (int) (totalCalories * 0.3 / 9);
					carbGramsEntry.setText(Integer.toString(carbGrams));
					proteinGramsEntry.setText(Integer.toString(proteinGrams));
					fatGramsEntry.setText(Integer.toString(fatGrams));
				}
				if (keto.isSelected()) {
					carbGrams = (int) (totalCalories * 0.15 / 4);
					proteinGrams = (int) (totalCalories * 0.15 / 4);
					fatGrams = (int) (totalCalories * 0.7 / 9);
					carbGramsEntry.setText(Integer.toString(carbGrams));
					proteinGramsEntry.setText(Integer.toString(proteinGrams));
					fatGramsEntry.setText(Integer.toString(fatGrams));
				}
				if (paleo.isSelected()) {
					carbGrams = (int) (totalCalories * 0.3 / 4);
					proteinGrams = (int) (totalCalories * 0.3 / 4);
					fatGrams = (int) (totalCalories * 0.4 / 9);
					carbGramsEntry.setText(Integer.toString(carbGrams));
					proteinGramsEntry.setText(Integer.toString(proteinGrams));
					fatGramsEntry.setText(Integer.toString(fatGrams));
				}
				if (lowfat.isSelected()) {
					carbGrams = (int) (totalCalories * 0.5 / 4);
					proteinGrams = (int) (totalCalories * 0.35 / 4);
					fatGrams = (int) (totalCalories * 0.15 / 9);
					carbGramsEntry.setText(Integer.toString(carbGrams));
					proteinGramsEntry.setText(Integer.toString(proteinGrams));
					fatGramsEntry.setText(Integer.toString(fatGrams));
				}
				
			}
			
			tdeeCalc.setText(String.valueOf(tdee));
			totalCalc.setText(String.valueOf(totalCalories));
		}

	public void calculateBFP() {
		
		double bfpresult = 0;
		
		if (malebfp.isSelected()) {
			bfpHeight = Double.parseDouble(bfpheightEntry.getText());
			bfpNeck = Double.parseDouble(neckEntry.getText());
			bfpWaist = Double.parseDouble(waistEntry.getText());
			bfpresult = 86.010 * Math.log10((bfpWaist - bfpNeck)) - 
					70.041 * Math.log10(bfpHeight) + 36.76;
			bfpEntry.setText(Double.toString(bfpresult));
			stage2.close();
		}
		if (femalebfp.isSelected()) {
			bfpHeight = Double.parseDouble(bfpheightEntry.getText());
			bfpNeck = Double.parseDouble(neckEntry.getText());
			bfpWaist = Double.parseDouble(waistEntry.getText());
			bfpHips = Double.parseDouble(hipsEntry.getText());
			bfpresult = 163.205 * Math.log10((bfpWaist + bfpHips - bfpNeck)) - 
					97.684 * Math.log10(bfpHeight) + 36.76;
			bfpEntry.setText(Double.toString(bfpresult));
			stage2.close();
		}
		
	}
	
	public static void main(String[] args) {
		Application.launch(args);
	}
}

